import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { auth } from "./actions/user";
import Auth from "./pages/auth";
import Main from "./pages/main";

const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(auth());
  }, []);

  const isAuth = useSelector((state) => state.user.isAuth);

  return isAuth ? <Main /> : <Auth />;
};

export default App;
