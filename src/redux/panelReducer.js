const SET_STUDENT_LIST = "SET_STUDENT_LIST";

const initialState = {
  studentList: [],
};

export const panelReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_STUDENT_LIST:
      return {
        ...state,
        studentList: action.payload,
      };
    default:
      return state;
  }
};

export const setStudentList = (data) => ({
  type: SET_STUDENT_LIST,
  payload: data,
});
