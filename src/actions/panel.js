import config from "../config/";
import { setStudentList } from "../redux/panelReducer";

export const showStudents = (kr, group) => {
  return async (dispatch) => {
    try {
      const url = `${config.SERVER_URL}/api/v1/panel/students?test=${kr}&goup=${group}`;

      const response = await fetch(url, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      });

      if (response.status === 200) {
        dispatch(setStudentList(JSON.parse(response.data)));
      }
    } catch (e) {
      const message = e.response;
      console.log(message);
      return e;
    }
  };
};
