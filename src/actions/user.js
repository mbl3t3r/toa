import axios from "axios";
import config from "../config";
import { setUser, logout } from "../redux/userReducer";

export const logoutAction = () => {
  return async (dispatch) => {
    dispatch(logout());
  };
};

export const registration = (data, isTeacher) => {
  return async (dispatch) => {
    try {
      const url = `${config.SERVER_URL}/api/v1/signup`;

      let response;

      if (isTeacher) {
        response = await axios.post(url, {
          login: data.username,
          name: data.name,
          surname: data.surname,
          password: data.password,
          role: "teacher",
          teacher_key: data.code,
        });
      } else {
        response = await axios.post(url, {
          login: data.username,
          name: data.name,
          surname: data.surname,
          password: data.password,
          group: data.group,
          role: "student",
        });
      }

      if (response.status === 201) {
        localStorage.setItem("accessToken", response.data.access_token);
        localStorage.setItem("refreshToken", response.data.refresh_token.UUID);
        dispatch(setUser(response.data.profile));
      }
    } catch (e) {
      const message = e.response.data;
      console.error(message);
      return e;
    }
  };
};

export const login = (username, password) => {
  return async (dispatch) => {
    try {
      const url = `${config.SERVER_URL}/api/v1/signin`;

      const response = await axios.post(url, {
        login: username,
        password,
      });

      if (response.status === 200) {
        localStorage.setItem("accessToken", response.data.access_token);
        localStorage.setItem("refreshToken", response.data.refresh_token.UUID);

        dispatch(setUser(response.data.profile));
      }
    } catch (e) {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("refreshToken");
      const message = e.response.data;
      console.error(message);
      return e;
    }
  };
};

export const auth = () => {
  return async (dispatch) => {
    try {
      const url = `${config.SERVER_URL}/api/v1/profile/auth`;

      const response = await axios.get(url, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      });
      if (response.status === 200) {
        dispatch(setUser(response.data));
      }
    } catch (e) {
      if (true) {
        try {
          const url = `${config.SERVER_URL}/api/v1/refresh`;

          const response = await axios.post(url, {
            refresh_token: localStorage.getItem("refreshToken"),
          });

          if (response.status === 201) {
            localStorage.setItem(
              "refreshToken",
              response.data.refresh_token.UUID
            );
            localStorage.setItem("accessToken", response.data.access_token);
            dispatch(setUser(response.data));
          }
        } catch (e) {
          console.error(e.response);
          return e;
        }
      }
      return e;
    }
  };
};

export const testResult = (data, testId) => {
  return async (dispatch) => {
    try {
      const url = `${config.SERVER_URL}/api/v1/profile/testresult?test_id=${testId}`;

      const response = await axios.post(
        url,
        {
          test_result: JSON.stringify(data),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
          },
        }
      );

      if (response.status === 201) {
        console.log("success");
      }
    } catch (e) {
      const message = e.response.data;
      console.error(message);
      return e;
    }
  };
};
