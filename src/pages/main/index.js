import React, { useState } from "react";
import { Route, Routes, Link, Navigate } from "react-router-dom";
import { Layout, Menu } from "antd";
import {
  PieChartOutlined,
  ExperimentOutlined,
  UserOutlined,
} from "@ant-design/icons";
import "./layout.css";
import s from "./index.module.scss";
import { useSelector, useDispatch } from "react-redux";
import Kr2 from "./components/Simulators/Kr2";
import Profile from "../profile";
import { logoutAction } from "../../actions/user";
import Root from "../root";
import Panel from "../panel";

const { Content, Sider } = Layout;
const { SubMenu } = Menu;

const Main = () => {
  const dispatch = useDispatch();
  const [collapsed, setCollapsed] = useState(false);
  const isAdmin =
    "teacher" === useSelector((state) => state.user.currentUser.role)
      ? true
      : false;

  const onCollapse = () => {
    setCollapsed((state) => !state);
  };

  const logoutUser = () => {
    dispatch(logoutAction());
  };

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={onCollapse}
        className={s.panel}
      >
        <div className="logo" />
        <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
          {isAdmin && (
            <Menu.Item key="panel" icon={<PieChartOutlined />}>
              <Link to="/panel">Панель управления</Link>
            </Menu.Item>
          )}
          <SubMenu
            key="simulators"
            icon={<ExperimentOutlined />}
            title="Тренажеры"
          >
            <Menu.Item key="kr1">
              <Link to="/simulators/kr1">КР 1</Link>
            </Menu.Item>
            <Menu.Item key="kr2">
              <Link to="/simulators/kr2">КР 2</Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu key="profile" icon={<UserOutlined />} title="Профиль">
            <Menu.Item key="profileItem">
              <Link to="/profile">Профиль</Link>
            </Menu.Item>
            <Menu.Item key="logout" onClick={logoutUser}>
              {<Link to="/login">Выйти</Link>}
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: "0 16px" }}>
          <Routes>
            <Route path="/panel" element={<Panel />} />
            <Route path="/simulators/kr2" element={<Kr2 />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/" exact element={<Root />} />
            <Route path="*" element={<Navigate to="/" replace />} />
          </Routes>
        </Content>
      </Layout>
    </Layout>
  );
};

export default Main;
