import React, { useRef, useState } from "react";
import Info from "../Info";
import { Form, Input, Button } from "antd";
import binary from "s-binary";
import s from "./index.module.scss";
import cn from "classnames";
import deepEqual from "deep-equal";
import { CheckCircleFilled, CloseCircleFilled } from "@ant-design/icons";
import { testResult } from "../../../../../actions/user";
import { useDispatch } from "react-redux";

const toBinaryPositive = (num) => {
  const value = (num >>> 0).toString(2);
  const def = 8 - value.length;
  let nulls = "";
  for (let i = 0; i < def; i++) nulls += "0";
  return nulls + value;
};

const convertToBinary = (num) => {
  if (num >= 0) {
    return toBinaryPositive(num);
  }

  const abs = Math.abs(Number(num));
  const digits = toBinaryPositive(abs).split("");
  digits[0] = "1";

  return digits.join("");
};

const convertToReverse = (num) => {
  if (num[0] === "1") {
    let result = "1";
    for (let i = 1; i < num.length; i++) {
      num[i] === "0" ? (result += "1") : (result += "0");
    }
    return result;
  }
  return num;
};

const getAllVariations = (a, b) => {
  const result = {
    a: {
      dec: null,
      positive: { direct: null, reverse: null, additional: null },
      negative: { direct: null, reverse: null, additional: null },
    },
    b: {
      dec: null,
      positive: { direct: null, reverse: null, additional: null },
      negative: { direct: null, reverse: null, additional: null },
    },
  };

  result.a.dec = Number(a);
  result.b.dec = Number(b);

  result.a.positive.direct =
    result.a.positive.reverse =
    result.a.positive.additional =
      convertToBinary(result.a.dec);
  result.b.positive.direct =
    result.b.positive.reverse =
    result.b.positive.additional =
      convertToBinary(result.b.dec);

  result.a.negative.direct = convertToBinary(-result.a.dec);
  result.b.negative.direct = convertToBinary(-result.b.dec);

  result.a.negative.reverse = convertToReverse(result.a.negative.direct);
  result.b.negative.reverse = convertToReverse(result.b.negative.direct);

  result.a.negative.additional = binary.complement(result.a.positive.direct);
  result.b.negative.additional = binary.complement(result.b.positive.direct);

  return result;
};

const multiplicationPowerOfTwo = (a, b) => {
  const result = {
    a: { minusTwo: null, three: null, four: null },
    b: { minusTwo: null, three: null, four: null },
  };

  result.a.minusTwo = binary.toBinary(a * Math.pow(2, -2), 9);
  result.a.three = binary.toBinary(a * Math.pow(2, 3), 8);
  result.a.four = binary.toBinary(a * Math.pow(2, 4), 8);

  result.b.minusTwo = binary.toBinary(b * Math.pow(2, -2), 9);
  result.b.three = binary.toBinary(b * Math.pow(2, 3), 8);
  result.b.four = binary.toBinary(b * Math.pow(2, 4), 8);

  return result;
};

const initial = {
  title: "Контрольная работа № 2",
  description: "Контрольная работа № 2 включает следующее задание.",
  points: [
    {
      number: 1,
      text: "Представьте в восьмиразрядной сетке числа А, В, −А, −В, используя прямой, обратный и дополнительный коды. Результаты представьте в таблице, левый столбец которой соответствует заданному числу, а оставшиеся три — его представлению в прямом, обратном и дополнительном кодах.",
    },
    {
      number: 2,
      text: "Умножьте числа А и В на 2 в степени −2, +3, +4. Результат представьте в предыдущей таблице.",
    },
    {
      number: 3,
      text: "Выполните следующие примеры:[A]д+[B]д, [−A]д+[B]д, [A]o+[−B]o, [−A]o+[−B]o;[A]п*[B]п любым способом.",
    },
  ],
};

const resultCheater = {
  part1: {
    a: {
      dec: "1",
      positive: {
        direct: "00000001",
        reverse: "00000001",
        additional: "00000001",
      },
      negative: {
        direct: "10000001",
        reverse: "11111110",
        additional: "11111111",
      },
    },
    b: {
      dec: "2",
      positive: {
        direct: "00000010",
        reverse: "00000010",
        additional: "00000010",
      },
      negative: {
        direct: "10000010",
        reverse: "11111101",
        additional: "11111110",
      },
    },
  },
  part2: {
    a: {
      minusTwo: "000000.01",
      three: "00001000",
      four: "00010000",
    },
    b: {
      minusTwo: "0000000.1",
      three: "00010000",
      four: "00100000",
    },
  },
  part3: {
    ex1: "00000011",
    ex2: "00000001",
    ex3: "11111110",
    ex4: "11111100",
    ex5: "00000010",
  },
};

const Kr2 = () => {
  const [form] = Form.useForm();

  const dispatch = useDispatch();

  const [isCorrect, setIsCorrect] = useState(false);
  const [isUnCorrect, setIsUncorrect] = useState(false);

  const [isCorrectMultiply, setIsCorrectMultiply] = useState(false);
  const [isUncorrectMultiply, setIsUncorrectMultiply] = useState(false);

  const [isCorrectMath, setIsCorrectMath] = useState(false);
  const [isUncorrectMath, setIsUncorrectMath] = useState(false);

  const [a, setA] = useState("");
  const [b, setB] = useState("");

  const [isFinish, setIsFinish] = useState(false);
  const [isFinishMulti, setIsFinishMulti] = useState(false);
  const [isFinishMath, setIsFinishMath] = useState(false);

  const [results, setResults] = useState({});

  const [showTable, setShowTable] = useState(false);

  const userDirectPositiveA = useRef(null);
  const userDirectPositiveB = useRef(null);

  const userReversePositiveA = useRef(null);
  const userReversePositiveB = useRef(null);

  const userAdditionalPositiveA = useRef(null);
  const userAdditionalPositiveB = useRef(null);

  const userDirectNegativeA = useRef(null);
  const userDirectNegativeB = useRef(null);

  const userReverseNegativeA = useRef(null);
  const userReverseNegativeB = useRef(null);

  const userAdditionalNegativeA = useRef(null);
  const userAdditionalNegativeB = useRef(null);

  const userMultiMinusTwoA = useRef(null);
  const userMultiThreeA = useRef(null);
  const userMultiFourA = useRef(null);

  const userMultiMinusTwoB = useRef(null);
  const userMultiThreeB = useRef(null);
  const userMultiFourB = useRef(null);

  const userMath1 = useRef(null);
  const userMath2 = useRef(null);
  const userMath3 = useRef(null);
  const userMath4 = useRef(null);
  const userMath5 = useRef(null);

  const [resPart1, setResPart1] = useState({});
  const [resPart2, setResPart2] = useState({});
  const [resPart3, setResPart3] = useState({});

  const onFinish = (values) => {
    const a = values.valueA;
    const b = values.valueB;

    setA(a);
    setB(b);

    setResults(getAllVariations(a, b));
    setShowTable(true);
    form.resetFields();
  };

  const finishHandler = () => {
    setIsCorrect(false);
    setIsUncorrect(false);
    setIsFinish((state) => !state);

    if (!isFinish) {
      const userResult = {
        a: {
          dec: null,
          positive: { direct: null, reverse: null, additional: null },
          negative: { direct: null, reverse: null, additional: null },
        },
        b: {
          dec: null,
          positive: { direct: null, reverse: null, additional: null },
          negative: { direct: null, reverse: null, additional: null },
        },
      };

      userResult.a.dec = a;
      userResult.a.positive.direct = userDirectPositiveA.current.value;
      userResult.a.positive.reverse = userReversePositiveA.current.value;
      userResult.a.positive.additional = userAdditionalPositiveA.current.value;
      userResult.a.negative.direct = userDirectNegativeA.current.value;
      userResult.a.negative.reverse = userReverseNegativeA.current.value;
      userResult.a.negative.additional = userAdditionalNegativeA.current.value;

      userResult.b.dec = b;
      userResult.b.positive.direct = userDirectPositiveB.current.value;
      userResult.b.positive.reverse = userReversePositiveB.current.value;
      userResult.b.positive.additional = userAdditionalPositiveB.current.value;
      userResult.b.negative.direct = userDirectNegativeB.current.value;
      userResult.b.negative.reverse = userReverseNegativeB.current.value;
      userResult.b.negative.additional = userAdditionalNegativeB.current.value;

      console.log(results);

      if (deepEqual(results, userResult)) {
        setIsCorrect(true);
        setResPart1(userResult);
      } else {
        setIsUncorrect(true);
      }
    }
  };

  const finishMultiplyHandler = () => {
    setIsCorrectMultiply(false);
    setIsUncorrectMultiply(false);

    setIsFinishMulti((state) => !state);

    if (!isFinishMulti) {
      const result = multiplicationPowerOfTwo(a, b);
      const userResult = {
        a: { minusTwo: null, three: null, four: null },
        b: { minusTwo: null, three: null, four: null },
      };

      userResult.a.minusTwo = userMultiMinusTwoA.current.value;
      userResult.a.three = userMultiThreeA.current.value;
      userResult.a.four = userMultiFourA.current.value;

      userResult.b.minusTwo = userMultiMinusTwoB.current.value;
      userResult.b.three = userMultiThreeB.current.value;
      userResult.b.four = userMultiFourB.current.value;

      console.log(result);

      if (deepEqual(result, userResult)) {
        setIsCorrectMultiply(true);
        setIsFinishMulti(true);
        setResPart2(userResult);
      } else {
        setIsUncorrectMultiply(true);
      }
    }
  };

  const finishMathHandler = () => {
    setIsCorrectMath(false);
    setIsUncorrectMath(false);

    setIsFinishMath((state) => !state);

    if (!isFinishMath) {
      const result = {};

      result.ex1 = binary.toBinary(Number(a) + Number(b), 8);

      result.ex2 =
        Number(-a) + Number(b) < 0
          ? binary.complement(
              binary.toBinary(Math.abs(Number(-a) + Number(b)), 8)
            )
          : binary.toBinary(Number(-a) + Number(b), 8);
      result.ex3 = binary.not(binary.toBinary(Number(a) - Number(b), 8));
      result.ex4 = binary.not(binary.toBinary(Number(-a) + Number(-b), 8));
      result.ex5 = binary.toBinary(Number(a) * Number(b), 8);

      const userResult = {};

      userResult.ex1 = userMath1.current.value;
      userResult.ex2 = userMath2.current.value;
      userResult.ex3 = userMath3.current.value;
      userResult.ex4 = userMath4.current.value;
      userResult.ex5 = userMath5.current.value;

      console.log(result);

      if (deepEqual(result, userResult)) {
        setIsCorrectMath(true);
        setIsFinishMath(true);
        setResPart3(userResult);
      } else {
        setIsUncorrectMath(true);
      }
    }
  };

  const finish = () => {
    const result = {};

    result.part1 = resPart1;
    result.part2 = resPart2;
    result.part3 = resPart3;

    console.log(result);
    dispatch(testResult(result, 2));
  };

  return (
    <>
      <Info
        title={initial.title}
        description={initial.description}
        points={initial.points}
      />
      <h2>Выполнение задания</h2>
      <Form onFinish={onFinish} className={s.numbers} form={form}>
        <Form.Item
          className={s.formItem}
          name="valueA"
          rules={[
            {
              required: true,
              message: "Введите A",
            },
          ]}
        >
          <Input autoComplete="off" type="number" placeholder="Введите А" />
        </Form.Item>

        <Form.Item
          className={s.formItem}
          name="valueB"
          rules={[
            {
              required: true,
              message: "Введите B",
            },
          ]}
        >
          <Input autoComplete="off" type="number" placeholder="Введите B" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Подтвердить
          </Button>
        </Form.Item>
      </Form>

      {showTable && (
        <>
          <div className={s.tables}>
            <div className={cn(s.table, s.formItem, isFinish && s.finishInput)}>
              <div className={s.tHeader}>Число</div>
              <div className={s.tHeader}>Прямой</div>
              <div className={s.tHeader}>Обратный</div>
              <div className={s.tHeader}>Дополнительный</div>
              <div className={s.inputNum}>{a}</div>
              <input
                autoComplete="off"
                type="number"
                ref={userDirectPositiveA}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userReversePositiveA}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userAdditionalPositiveA}
              />
              <div className={s.inputNum}>{b}</div>
              <input
                autoComplete="off"
                type="number"
                ref={userDirectPositiveB}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userReversePositiveB}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userAdditionalPositiveB}
              />
              <div className={s.inputNum}>{-Number(a)}</div>
              <input
                autoComplete="off"
                type="number"
                ref={userDirectNegativeA}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userReverseNegativeA}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userAdditionalNegativeA}
              />
              <div className={s.inputNum}>{-Number(b)}</div>
              <input
                autoComplete="off"
                type="number"
                ref={userDirectNegativeB}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userReverseNegativeB}
              />
              <input
                autoComplete="off"
                type="number"
                ref={userAdditionalNegativeB}
              />
            </div>
            {isCorrect && (
              <div className={cn(s.table2, isFinishMulti && s.finishInput)}>
                <div className={s.tHeader}>×2^-2</div>
                <div className={s.tHeader}>×2^3</div>
                <div className={s.tHeader}>×2^4</div>
                <input
                  type="text"
                  autoComplete="off"
                  ref={userMultiMinusTwoA}
                />
                <input type="text" autoComplete="off" ref={userMultiThreeA} />
                <input type="text" autoComplete="off" ref={userMultiFourA} />
                <input
                  type="text"
                  autoComplete="off"
                  ref={userMultiMinusTwoB}
                />
                <input type="text" autoComplete="off" ref={userMultiThreeB} />
                <input type="text" autoComplete="off" ref={userMultiFourB} />
              </div>
            )}
          </div>
          <Button onClick={finishHandler} type="link">
            {!isFinish ? <>Продолжить</> : <>Изменить</>}
            {isCorrect && (
              <CheckCircleFilled style={{ color: "rgb(20, 161, 1)" }} />
            )}
            {isUnCorrect && (
              <CloseCircleFilled style={{ color: "rgb(221, 0, 0)" }} />
            )}
          </Button>
          {isCorrect && (
            <Button onClick={finishMultiplyHandler}>
              {!isFinishMulti ? <>Подтвердить</> : <>Изменить</>}
              {isCorrectMultiply && (
                <CheckCircleFilled style={{ color: "rgb(20, 161, 1)" }} />
              )}
              {isUncorrectMultiply && (
                <CloseCircleFilled style={{ color: "rgb(221, 0, 0)" }} />
              )}
            </Button>
          )}
        </>
      )}

      {isFinishMulti && isCorrectMultiply && (
        <>
          <div className={cn(s.tableMath, isFinishMath && s.finishInput)}>
            <div>[A]д+[B]д</div>
            <input type="text" ref={userMath1} />
            <div>[−A]д+[B]д</div>
            <input type="text" ref={userMath2} />
            <div>[A]o+[−B]o</div>
            <input type="text" ref={userMath3} />
            <div>[−A]o+[−B]o</div>
            <input type="text" ref={userMath4} />
            <div>[A]п*[B]п</div>
            <input type="text" ref={userMath5} />
          </div>
          <Button onClick={finishMathHandler} style={{ marginTop: "10px" }}>
            {!isFinishMath ? <>Подтвердить</> : <>Изменить</>}
            {isCorrectMath && (
              <CheckCircleFilled style={{ color: "rgb(20, 161, 1)" }} />
            )}
            {isUncorrectMath && (
              <CloseCircleFilled style={{ color: "rgb(221, 0, 0)" }} />
            )}
          </Button>
        </>
      )}

      {isFinishMath && isCorrectMath && (
        <>
          <h1 style={{ marginTop: "30px" }}>Тест пройден</h1>
          <Button
            type="primary"
            onClick={finish}
            style={{ marginBottom: "50px" }}
          >
            Отправить результаты
          </Button>
        </>
      )}

      {/* <Button type="primary" onClick={finish} style={{ marginBottom: "50px" }}>
        Отправить результаты
      </Button> */}
    </>
  );
};

export default Kr2;
