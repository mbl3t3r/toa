import React from "react";
import s from "./index.module.scss";

const Info = ({ title, description, points }) => {
  return (
    <section className={s.info}>
      <h1 className={s.title}>{title}</h1>
      <p>{description}</p>
      {points.map((point) => (
        <div className={s.point} key={point.number}>
          <span className={s.number}>{point.number}.</span>
          <span>{point.text}</span>
        </div>
      ))}
    </section>
  );
};

export default Info;
