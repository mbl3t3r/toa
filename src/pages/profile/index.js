import React from "react";
import { useSelector } from "react-redux";
import { UserOutlined } from "@ant-design/icons";
import { Avatar } from "antd";
import s from "./index.module.scss";

const Profile = () => {
  const login = useSelector((state) => state.user.currentUser.login);
  const name = useSelector((state) => state.user.currentUser.name);
  const surname = useSelector((state) => state.user.currentUser.surname);
  const group = useSelector((state) => state.user.currentUser.group);
  const role = useSelector((state) => state.user.currentUser.role);

  return (
    <div className={s.profile}>
      <div className={s.info}>
        <Avatar size={80} icon={<UserOutlined />} />
        <div className={s.user}>
          <h1>
            {name} {surname}
          </h1>
          <p>{login}</p>
        </div>
      </div>
      {group && <p>Группа: {group}</p>}
      <div className={s.role}>
        <h3>Тип профиля:</h3>{" "}
        {role === "teacher" ? (
          <span className={s.teach}>Преподаватель</span>
        ) : (
          <span className={s.stud}>Студент</span>
        )}
      </div>
    </div>
  );
};

export default Profile;
