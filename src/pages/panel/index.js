import React, { useState } from "react";
import { Button, Select } from "antd";
import s from "./index.module.scss";
import { useDispatch } from "react-redux";
import { showStudents } from "../../actions/panel";

const { Option } = Select;

const Panel = () => {
  const [kr, setKr] = useState("");
  const [group, setGroup] = useState("");

  const dispatch = useDispatch();

  const changeKr = (value) => {
    setKr(() => value);
  };

  const changeGroup = (value) => {
    setGroup(() => value);
  };

  const showResults = () => {
    const filters = {
      kr,
      group,
    };
    console.log(filters);
    dispatch(showStudents(filters.kr, filters.group));
  };

  return (
    <div className={s.panel}>
      <div className={s.filters}>
        <Select
          defaultValue="КР"
          style={{ width: 120 }}
          onChange={changeKr}
          className={s.option}
        >
          <Option value="1" disabled>
            КР1
          </Option>
          <Option value="2">КР2</Option>
        </Select>

        <Select
          defaultValue="Группа"
          style={{ width: 120 }}
          onChange={changeGroup}
          className={s.option}
        >
          <Option value="К3-84Б">К3-84Б</Option>
        </Select>
        <Button type="primary" onClick={showResults}>
          Показать
        </Button>
      </div>
    </div>
  );
};

export default Panel;
