import React from "react";
import Register from "./components/register";
import Login from "./components/login";
import s from "./index.module.scss";
import { Navigate, Route, Routes } from "react-router";

const Auth = () => {
  return (
    <div className={s.auth}>
      <Routes>
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<Navigate to="/login" replace />} />
      </Routes>
    </div>
  );
};

export default Auth;
