import { Form, Input, Button, Switch } from "antd";
import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { registration } from "../../../../actions/user";
import s from "./index.module.scss";

const Register = () => {
  const [teacher, setTeacher] = useState(false);
  const code = useRef(null);

  const dispatch = useDispatch();

  const onFinish = (data) => {
    dispatch(registration(data, teacher));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const isTeacher = (checked) => {
    setTeacher(checked);
  };

  return (
    <div className={s.register}>
      <h1>Регистрация</h1>
      <h3>Зарегистрируйтесь, чтобы воспользоваться сервисом</h3>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: "Введите логин",
            },
          ]}
        >
          <Input placeholder="Логин" />
        </Form.Item>
        <Form.Item
          name="name"
          rules={[
            {
              required: true,
              message: "Введите имя",
            },
          ]}
        >
          <Input placeholder="Имя" />
        </Form.Item>
        <Form.Item
          name="surname"
          rules={[
            {
              required: true,
              message: "Введите фамилию",
            },
          ]}
        >
          <Input placeholder="Фамилия" />
        </Form.Item>
        {!teacher && (
          <Form.Item
            name="group"
            rules={[
              {
                required: true,
                message: "Введите группу",
              },
            ]}
          >
            <Input placeholder="Группа (пример: К3-84Б)" />
          </Form.Item>
        )}
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Введите пароль",
            },
          ]}
        >
          <Input.Password placeholder="Пароль" autoComplete="true" />
        </Form.Item>
        <Form.Item
          name="confirm"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Повторите пароль",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(new Error("Пароли не совпадают"));
              },
            }),
          ]}
        >
          <Input.Password placeholder="Повторите пароль" autoComplete="true" />
        </Form.Item>
        {teacher && (
          <Form.Item
            name="code"
            rules={[
              {
                required: true,
                message: "Введите код",
              },
            ]}
          >
            <Input placeholder="Код преподавателя" ref={code} />
          </Form.Item>
        )}
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Регистрация
          </Button>
        </Form.Item>
        <Form.Item>
          {/* <Checkbox onChange={isTeacher}>Я преподаватель</Checkbox> */}
          Я преподаватель <Switch size="small" onChange={isTeacher} />
        </Form.Item>
        Уже есть аккаунт? {<Link to="/login">Войдите</Link>}
      </Form>
    </div>
  );
};

export default Register;
