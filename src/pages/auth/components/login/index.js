import React from "react";
import s from "./index.module.scss";
import { login } from "../../../../actions/user";
import { Form, Input, Button } from "antd";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";

const Login = () => {
  const dispatch = useDispatch();

  const onFinish = (values) => {
    dispatch(login(values.username, values.password));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className={s.login}>
      <h1>Войти</h1>
      <h3>Войдите, чтобы воспользоваться сервисом</h3>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: "Введите логин",
            },
          ]}
        >
          <Input placeholder="Логин" />
        </Form.Item>

        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Введите пароль",
            },
          ]}
        >
          <Input.Password placeholder="Пароль" autoComplete="true" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Войти
          </Button>
        </Form.Item>
      </Form>
      Нет аккаунта? {<Link to="/register">Зарегистрируйтесь</Link>}
    </div>
  );
};

export default Login;
