import React from "react";
import { useSelector } from "react-redux";
import s from "./index.module.scss";

const Root = () => {
  const name = useSelector((state) => state.user.currentUser.name);
  return (
    <div className={s.root}>
      <h1>Добро пожаловать, {name}!</h1>
      <h2>Для начала работы, выберете тренажер из списка слева</h2>
    </div>
  );
};

export default Root;
